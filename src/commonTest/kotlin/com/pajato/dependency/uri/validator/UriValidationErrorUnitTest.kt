package com.pajato.dependency.uri.validator

import com.pajato.test.ReportingTestProfiler
import java.io.IOException
import kotlin.test.Test
import kotlin.test.assertFailsWith

class UriValidationErrorUnitTest : ReportingTestProfiler() {
    @Test fun `When a file creation error is thrown, detect it`() {
        assertFailsWith<UriValidationError> { throw UriValidationError(IOException()) }
    }
}
