package com.pajato.dependency.uri.validator

import com.pajato.test.ReportingTestProfiler
import java.io.File
import java.net.URL
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import kotlin.test.fail

class ValidateUriUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader
    private val message = "com.pajato.dependency.uri.validator.UriValidationError: " +
        "java.lang.IllegalArgumentException: The argument 'uri' is not a file URI!"
    private fun handler(cause: Throwable) { throw cause }

    @Test fun `When validating encounters an error, verify the exception`() {
        val url: URL = loader.getResource("not-a-file") ?: fail("Could not get resource!")
        assertFailsWith<UriValidationError> { validateUri(url.toURI(), ::handler) }
            .also { assertEquals(message, it.message) }
    }

    @Test fun `When a URI file does not exist, verify that it is created`() {
        val url = loader.getResource("file.txt") ?: fail("Could not create resource!")
        val file = File(url.toURI()).also { it.delete() }
        if (file.exists()) fail("The info file: ${file.absolutePath} could not be deleted!")
        validateUri(url.toURI(), ::handler)
        assertTrue(file.exists())
    }

    @Test fun `When validation succeeds, verify the behavior`() {
        val url: URL = loader.getResource("buffer.txt") ?: fail("Could not get resource!")
        validateUri(url.toURI(), ::handler)
    }
}
