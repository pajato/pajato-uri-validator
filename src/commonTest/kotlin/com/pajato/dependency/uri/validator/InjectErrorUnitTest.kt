package com.pajato.dependency.uri.validator

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertFailsWith

class InjectErrorUnitTest : ReportingTestProfiler() {
    @Test fun `When an inject error is thrown, detect it`() {
        assertFailsWith<InjectError> { throw InjectError("some message") }
    }
}
