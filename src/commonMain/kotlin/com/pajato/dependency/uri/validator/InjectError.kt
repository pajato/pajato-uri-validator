package com.pajato.dependency.uri.validator

public class InjectError(override val message: String?) : Exception(message)
