package com.pajato.dependency.uri.validator

public const val INVALID_INJECT_ERROR: String = "The argument 'uri' is not a file URI!"
