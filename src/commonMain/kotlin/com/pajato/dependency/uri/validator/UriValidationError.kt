package com.pajato.dependency.uri.validator

public class UriValidationError(cause: Throwable) : Throwable(cause)
