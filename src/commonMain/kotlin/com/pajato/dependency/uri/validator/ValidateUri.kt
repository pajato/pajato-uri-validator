package com.pajato.dependency.uri.validator

import java.io.File
import java.net.URI

public fun validateUri(uri: URI, errorHandler: (exc: Throwable) -> Unit) {
    val error: Throwable? = getError(uri)
    if (error != null) errorHandler(UriValidationError(error))
}

private fun getError(uri: URI): Throwable? {
    val file: File? = try { File(uri) } catch (exc: IllegalArgumentException) { return exc }
    if (file != null && !file.exists()) try { file.createFile() } catch (exc: Exception) { return exc }
    if (file != null && !file.isFile) return UriValidationError(IllegalArgumentException(INVALID_INJECT_ERROR))
    return null
}

private fun File.createFile() { try { createNewFile() } catch (exc: Exception) { throw UriValidationError(exc) } }
